# IRC server

build server: go build -o go-irc-server bitbucket.org/mnk1985/go-irc-server

start server: $GOPATH/src/bitbucket.org/mnk1985/go-irc-server/go-irc-server [port]

connect to server: nc localhost [port]
