package main

import (
	"fmt"
	"strings"
)

func handleJoinCommand(parameters []string, command *Command) {

	if len(parameters) == 0 {
		return
	}
	roomName := parameters[0]
	if strings.ContainsAny(roomName, "#") {
		roomName = roomName[1:]
	}
	room, err := getRoomFromName(roomName)
	if err != nil {
		room = &Room{name: roomName, clients: make(map[*Client]bool)}
	}
	room.clients[command.client] = true

	mutex.Lock()
	rooms[room] = true
	mutex.Unlock()
	command.client.room = room
	replyJoinCommand(command.client, room)
}

func handleListRooms(parameters []string, command *Command) {

	if len(rooms) == 0 {
		sendMessageFromServer(command.client, "no created rooms")
		return
	}

	client := command.client
	for room := range rooms {
		message := fmt.Sprintf("%s\n", room.name)
		client.conn.Write([]byte(message))
	}
}

func handleListUserts(parameters []string, command *Command) {

	client := command.client
	fmt.Printf("client: %+v"+crlf, client)

	if client.room.name == "" {
		sendMessageFromServer(client, "user has to join room to list its users")
		return
	}

	roomName := client.room.name
	fmt.Println("roomName: " + roomName)
	room, err := getRoomFromName(roomName)
	fmt.Printf("room: %+v"+crlf, room)
	if err != nil {
		handleInvalidRoom(command.client, roomName)
		return
	}
	printAllRoomUsers(room, client)
}

func handleLeaveCommand(parameters []string, command *Command) {

	client := command.client

	if client.room.name == "" {
		sendMessageFromServer(client, "you didn't join any room")
	}

	roomName := client.room.name
	room, err := getRoomFromName(roomName)
	if err != nil {
		handleInvalidRoom(command.client, roomName)
		return
	}
	message := fmt.Sprintf("%s left %s", command.client.nickname, roomName)
	fmt.Println(message)
	for roomClient := range room.clients {
		sendMessageFromServer(roomClient, message)
	}
	mutex.Lock()
	delete(room.clients, command.client)
	mutex.Unlock()
}

func handleNickCommand(parameters []string, command *Command) {

	if len(parameters) != 1 {
		command.client.conn.Write([]byte("Invalid syntax\n"))
		return
	}

	nickname := parameters[0]
	_, err := getClientFromName(nickname)

	if err != nil {
		command.client.nickname = nickname
		showNick(command.client)
	} else {
		message := fmt.Sprintf("Nickname %s is already in use", nickname)
		sendMessageFromServer(command.client, message)
	}
	fmt.Println("handleNickCommand")
}

func handleMessageCommand(parameters []string, command *Command) {
	if len(parameters) < 2 {
		return
	}
	message := strings.Join(parameters[1:], " ")
	roomName := parameters[0][1:]
	room, err := getRoomFromName(roomName)
	if err != nil {
		handleInvalidRoom(command.client, roomName)
		return
	}
	_, present := room.clients[command.client]
	if present != true {
		command.client.conn.Write([]byte("Not part of this channel\n"))
		return
	}

	sendMessageToAllRoomUsers(room, command.client, message)
}

func handlePrivateMessageCommand(parameters []string, command *Command) {

	fmt.Printf("parameters: %+v"+crlf, parameters)
	if len(parameters) < 1 {
		return
	}
	message := strings.Join(parameters[1:], " ")
	clientName := parameters[0]
	client, err := getClientFromName(clientName)
	if err != nil {
		sendMessageFromServer(command.client, "couldnt find user:"+client.nickname)
	}
	message = fmt.Sprintf("(PRIVMSG) %s", message)
	fmt.Println(message)
	sendMessageFromUser(command.client, client, message)
}

func handleInvalidCommand(command *Command) {
	command.client.conn.Write([]byte("Invalid commmand\n"))
}

func handleInvalidRoom(client *Client, roomName string) {
	client.conn.Write([]byte("no users found in " + roomName + crlf))
}

func handleListCommands(parameters []string, command *Command) {
	showAllCommands(command.client)
}
