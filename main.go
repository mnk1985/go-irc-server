package main

import (
	"fmt"
	"net"
	"os"
	"strings"
	"sync"

	"bufio"

	"github.com/icrowley/fake"
)

const (
	serverName   = "irc.server"
	crlf         = "\r\n"
	join         = "JOIN"
	leave        = "LEAVE"
	nick         = "NICK"
	privmsg      = "PRIVMSG"
	msg          = "MSG"
	listRooms    = "ROOMS"
	listUsers    = "USERS"
	listCommands = "HELP"
)

var clients = make(map[*Client]bool)
var rooms = make(map[*Room]bool)
var mutex = &sync.Mutex{}

type handleCommand func([]string, *Command)

var commandMap = map[string]handleCommand{
	join:         handleJoinCommand,
	leave:        handleLeaveCommand,
	nick:         handleNickCommand,
	privmsg:      handlePrivateMessageCommand,
	msg:          handleMessageCommand,
	listRooms:    handleListRooms,
	listUsers:    handleListUserts,
	listCommands: handleListCommands,
}

var commandDescription = map[string]string{
	join:         join + " #[roomName] -- join roomName (or create if it does not exist yet)",
	leave:        leave + " -- leave current room",
	nick:         nick + " [nickname] -- change nick",
	privmsg:      privmsg + " [nickname] [message] -- send private message to user with nickname",
	msg:          msg + " #[roomName] -- send message to all room users",
	listRooms:    listRooms + " -- list all rooms",
	listUsers:    listUsers + " -- list all users in current room",
	listCommands: listCommands + " -- list all commands",
}

type Command struct {
	name              string
	client            *Client
	handleJoinCommand ([]string)
}

type Room struct {
	name    string
	clients map[*Client]bool
}

type Client struct {
	conn     net.Conn
	room     *Room
	nickname string
}

func parseCommand(command *Command) {

	tokens := strings.Split(command.name, " ")
	fmt.Printf("tokens: %+v"+crlf, tokens)

	commandName := tokens[0]
	parameters := tokens[1:]
	handleFunc, ok := commandMap[commandName]
	if !ok {
		if command.client.room.name != "" {
			sendMessageToAllRoomUsers(command.client.room, command.client, command.name)
		} else {
			handleInvalidCommand(command)
		}
	} else {
		handleFunc(parameters, command)
	}
}

func handleClient(client *Client, commandChan chan *Command) {

	showNick(client)
	showAllCommands(client)

	readerObj := bufio.NewReader(client.conn)
	for {
		message, _ := readerObj.ReadString('\n')
		if len(message) == 0 {
			continue
		}
		message = strings.TrimRight(message, crlf)
		if strings.Compare(strings.Split(message, " ")[0], "QUIT") == 0 {

			mutex.Lock()
			if client.room.name != "" {
				room, err := getRoomFromName(client.room.name)
				if err == nil {
					delete(room.clients, client)
				}
			}
			delete(clients, client)
			mutex.Unlock()
			break
		}
		fmt.Println("Message:", string(message))
		command := &Command{name: string(message), client: client}
		commandChan <- command
	}

}

func main() {

	commandChan := make(chan *Command)
	connectionChan := make(chan net.Conn)
	args := os.Args[1:]
	port := ":6667"
	if len(args) == 1 {
		port = ":" + args[0]
	}

	ln, _ := net.Listen("tcp", port)
	defer ln.Close()
	go func() {
		for {
			conn, err := ln.Accept()
			if err != nil {
				continue
			}
			fmt.Println("accepted new connection")
			connectionChan <- conn
		}
	}()
	for {
		select {
		case command := <-commandChan:
			go parseCommand(command)
		case conn := <-connectionChan:
			client := &Client{conn: conn, room: &Room{}, nickname: fake.Word()}
			mutex.Lock()
			clients[client] = true
			mutex.Unlock()
			go handleClient(client, commandChan)
		}
	}
}
