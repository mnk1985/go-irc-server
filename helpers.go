package main

import (
	"fmt"
	"strings"
)

func showNick(client *Client) {
	sendMessageFromServer(client, "your nick is "+client.nickname)
}

func sendMessageToAllRoomUsers(room *Room, client *Client, message string) {
	for clientFromRoom := range room.clients {
		if strings.Compare(clientFromRoom.nickname, client.nickname) != 0 {
			sendMessageFromUser(client, clientFromRoom, message)
		}
	}
}

func replyJoinCommand(client *Client, room *Room) {

	message := fmt.Sprintf("%s joined %s", client.nickname, room.name)
	fmt.Println(message)
	for roomClient := range room.clients {
		sendMessageFromServer(roomClient, message)
	}

	printAllRoomUsers(room, client)

}

func getRoomFromName(name string) (*Room, error) {
	for room := range rooms {
		if room.name == name {
			return room, nil
		}
	}
	return nil, fmt.Errorf("no room found with name: " + name + crlf)
}

func getClientFromName(name string) (*Client, error) {
	for client := range clients {
		if client.nickname == name {
			return client, nil
		}
	}
	return nil, fmt.Errorf("not found user: " + name)
}

func showAllCommands(client *Client) {
	message := "all commands" + crlf
	for _, commandDesc := range commandDescription {
		message += commandDesc + crlf
	}
	sendMessageFromServer(client, message)
}

func printAllRoomUsers(room *Room, client *Client) {
	client.conn.Write([]byte(room.name + " users: \n"))
	for clientFromRoom := range room.clients {
		client.conn.Write([]byte(clientFromRoom.nickname + crlf))
	}
}

func sendMessageFromServer(client *Client, message string) {
	client.conn.Write([]byte(serverName + ": " + message + crlf))
}

func sendMessageFromUser(fromUser *Client, toUser *Client, message string) {
	if fromUser.nickname == toUser.nickname {
		return
	}

	toUser.conn.Write([]byte(toUser.nickname + " >>> " + message + crlf))
}
